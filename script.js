// 3. Create a fetch request using the GET method that will retrieve all the

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(objects => {

	// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
	let arr = objects.map(values => values.title);
	console.log("todos");
	console.log(arr);
});



// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.


let todoId = prompt("Enter id:");
fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`)
.then(response => response.json())

// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

.then(data => {
	console.log(`Todo title: ${data.title}`);
	console.log(`Todo status: ${checkStatus(data.completed)}`);
});

function checkStatus(status){
	return status?  "Completed" : "Not completed";
}


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	header: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		userId: 1,
		id: 4,
		title: "Kain Tulog",
		completed: false
	})
})
.then(response => response.json())
.then(data => console.log(data))


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	header: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		completed: true
	})
})
.then(response => response.json())
.then(data => console.log(data))

// 9. Update a to do list item by changing the data structure to contain the following properties:
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	header: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "Kain tulog",
		description: "Trabaho ng tamad",
		status : true,
		date_completed: "10/3/2022",
		user_id: 1
	})
})
.then(response => response.json())
.then(data => console.log(data))


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	header: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		completed: true
	})
})
.then(response => response.json())
.then(data => console.log(data))

// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	header: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		completed: true,
		date_completed: "10/3/2022"
	})
})
.then(response => response.json())
.then(data => console.log(data))

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
});